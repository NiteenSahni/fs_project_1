let fs = require('fs')
let path = require("path")
function createAndDelete(dirName) {
    let folderName = dirName;
    let randomNameArray = Array(5).fill(0).map((n, index) => {
        return index
    })
    fs.mkdir(path.join(__dirname, `${folderName}`), (err, data) => {
        if (err) {
            console.log(err)
        } else {
            // console.log("dir created")
            fileCreate(err, randomNameArray)
        }
    })
    function fileCreate(err, nameArray) {
        if (err) {
            console.log(err);
        } else {
            for (let index = 0; index < nameArray.length; index++) {
                fs.writeFile(`${path.join(__dirname, `${folderName}`, `${nameArray[index]}.JSON`)}`, JSON.stringify({ 1: "Dummy-Data" }), (err, data) => {
                    if (err) {
                        console.log("error in file-creation", err);
                    } else {
                        console.log(`${nameArray[index]}.JSON ` + "created");
                        deleteFiles(err, nameArray[index])
                    }
                })
            }
        }

    }

    function deleteFiles(err, fileName) {
        if (err) {
            console.log(err);
        } else {
            fs.unlink(`${path.join(__dirname, `${folderName}`, `${fileName}.JSON`)}`, (err, data) => {
                if (err) {
                    console.log("error in file-deletion", err);
                } else {
                    console.log(`${fileName}.JSON ` + "Files Deleted")
                }
            })

        }
    }



}

module.exports = createAndDelete;


